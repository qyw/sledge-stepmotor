/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/** 
 * @author  Kyb Sledgehammer <i.kyb[2]ya,ru>
 * @brief	Software driver for stepper motor driver A4988
 */

#ifndef __A4988_H
#define __A4988_H

#ifdef __cplusplus
extern "C" {
#endif


enum a4988_Step {
	a4988_Step_1,
	a4988_Step_2,
	a4988_Step_4,
	a4988_Step_8,
	a4988_Step_16,
};

enum a4988_Direction {
	a4988_Direction_Forward,
	a4988_Direction_Reverse,
};

struct a4988_Settings {
	enum a4988_Step step;
	enum a4988_Direction dir;
};

#define a4988_Settings_DEFAULT  {a4988_Step_8, a4988_Direction_Forward}


void a4988_init( struct a4988_Settings *s );

/**
 * Задать микро шаг
 
 Table 1. Microstepping Resolution Truth Table
 | MS1 | MS2 | MS3 | Microstep Resolution Excitation Mode
 |  L  |  L  |  L  | Full Step 2 Phase
 |  H  |  L  |  L  | Half Step 1-2 Phase
 |  L  |  H  |  L  | Quarter Step W1-2 Phase
 |  H  |  H  |  L  | Eighth Step 2W1-2 Phase
 |  H  |  H  |  H  | Sixteenth Step 4W1-2 Phase
 */
int a4988_step_set( enum a4988_Step s );

/// Узнать микро шаг
enum a4988_Step a4988_step_get();


/// задать направление вращения
void a4988_dir_set( enum a4988_Direction d );


/// Узннать направление вращения
enum a4988_Direction a4988_dir_get();



#ifdef __cplusplus
}
#endif

#endif //__A4988_H
