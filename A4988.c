/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/** 
 * @author  Kyb Sledgehammer <i.kyb[2]ya,ru>
 * @brief	Software driver for stepper motor driver A4988
 */

#include "./A4988.h"
#include "sensor_self_test.h"
#include "Sledge/bsp.h"

static const GPIO_t * const pinENABLE = PC2, /// розовый. Normal LOW. When set to a logic HIGH, the outputs are disabled.
					* const pinMS1 = PF10,   /// голубой
					* const pinMS2 = PH4,    /// фиолетовый
					* const pinMS3 = PH2,    /// сиреневый
                    * const pinRESET = PC3,  /// жёлтый. Normal HIGH. All STEP inputs are ignored until the RESET input is set to HIGH. 
                    * const pinSLEEP = PA0,  /// серый. Normal HIGH. A logic LOW on the SLEEP pin puts the A4988 into Sleep mode
//					* const pinSTEP = PF8,   /// белый
					* const pinDIR = PF9;    /// зелёный

static struct a4988_Settings a4988_settings;


/// Initialize A4988
void a4988_init( struct a4988_Settings *s )
{
	GPIO_InitTypeDef gi; 
	gi.GPIO_Speed = GPIO_Speed_2MHz; 
	gi.GPIO_PuPd = GPIO_PuPd_UP;
	gi.GPIO_OType = GPIO_OType_PP;
	
	bsp_GpioOut_InitStruct( pinENABLE, &gi );
	bsp_GpioOut_InitStruct( pinMS1, &gi );
	bsp_GpioOut_InitStruct( pinMS2, &gi );
	bsp_GpioOut_InitStruct( pinMS3, &gi );
	bsp_GpioOut_InitStruct( pinRESET, &gi );
	bsp_GpioOut_InitStruct( pinSLEEP, &gi );
	//use sensor self test module. //bsp_GpioOut_InitStruct( pinSTEP );
	bsp_GpioOut_InitStruct( pinDIR, &gi );
	
	bsp_OutLow(pinENABLE);
	bsp_OutHigh(pinRESET);
	bsp_OutHigh(pinSLEEP);
	
	a4988_dir_set(s->dir);
	a4988_step_set(s->step);
	a4988_settings = *s;
}


/**
 * Задать микро шаг
 
 Table 1. Microstepping Resolution Truth Table
 | MS1 | MS2 | MS3 | Microstep Resolution Excitation Mode
 |  L  |  L  |  L  | Full Step 2 Phase
 |  H  |  L  |  L  | Half Step 1-2 Phase
 |  L  |  H  |  L  | Quarter Step W1-2 Phase
 |  H  |  H  |  L  | Eighth Step 2W1-2 Phase
 |  H  |  H  |  H  | Sixteenth Step 4W1-2 Phase
 */
int a4988_step_set( enum a4988_Step s )
{
	switch(s){
		case a4988_Step_1:  bsp_OutLow (pinMS1); bsp_OutLow (pinMS2); bsp_OutLow (pinMS3); break;
		case a4988_Step_2:  bsp_OutHigh(pinMS1); bsp_OutLow (pinMS2); bsp_OutLow (pinMS3); break;
		case a4988_Step_4:  bsp_OutLow (pinMS1); bsp_OutHigh(pinMS2); bsp_OutLow (pinMS3); break;
		case a4988_Step_8:  bsp_OutHigh(pinMS1); bsp_OutHigh(pinMS2); bsp_OutLow (pinMS3); break;
		case a4988_Step_16: bsp_OutHigh(pinMS1); bsp_OutHigh(pinMS2); bsp_OutHigh(pinMS3); break;
///\todo		case a4988_Step_32: bsp_OutHigh(pinMS1); bsp_OutHigh(pinMS2); bsp_OutHigh(pinMS3); break;  // for DRV8825
	}
	a4988_settings.step = s;
	return 0;
}

/**
 * Узнать микро шаг
 */
enum a4988_Step a4988_step_get()
{
	return a4988_settings.step;
}


/**
 * задать направление вращения
 */
void a4988_dir_set( enum a4988_Direction d )
{
	switch(d){
		case a4988_Direction_Forward: bsp_OutLow (pinDIR); break;
		case a4988_Direction_Reverse: bsp_OutHigh(pinDIR); break;
	}
	a4988_settings.dir = d;
}

/**
 * Узннать направление вращения
 */
enum a4988_Direction a4988_dir_get()
{
	return a4988_settings.dir;
}
